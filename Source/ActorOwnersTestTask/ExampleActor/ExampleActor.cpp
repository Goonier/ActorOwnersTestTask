// Fill out your copyright notice in the Description page of Project Settings.


#include "ExampleActor.h"
#include "Kismet/GameplayStatics.h"
#include "Game/Save/ExampleActorSaveGame.h"
#include "Game/TestTaskGameInstance.h"

// Sets default values
AExampleActor::AExampleActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	ExampleMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ActorMesh"));
	ExampleMesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AExampleActor::BeginPlay()
{
	Super::BeginPlay();
	
	CheckSaveGame();
}

void AExampleActor::SaveExampleActor()
{
	UGameplayStatics::SaveGameToSlot(ExampleActorSaveObject, this->GetName(), 0);
}

void AExampleActor::CheckSaveGame()
{
	bool SaveGameExist = UGameplayStatics::DoesSaveGameExist(this->GetName(), 0);

	if (SaveGameExist)
	{
		auto SaveGame = Cast<UExampleActorSaveGame>(UGameplayStatics::LoadGameFromSlot(this->GetName(), 0));
		if (SaveGame)
		{
			ExampleActorSaveObject = SaveGame;
		}
	}
	else
	{
		ExampleActorSaveObject = Cast<UExampleActorSaveGame>(UGameplayStatics::CreateSaveGameObject(UExampleActorSaveGame::StaticClass()));
		
		auto GI = Cast<UTestTaskGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));

		if (GI)
		{
			FOwnerInfo FoundOwner = GI->FindOwnerInfoByID(DefaultOwnerID);
			if (FoundOwner.ID != "None")
				ExampleActorSaveObject->OwnerInfo = FoundOwner;
	

			SaveExampleActor();
		}
	}
}

