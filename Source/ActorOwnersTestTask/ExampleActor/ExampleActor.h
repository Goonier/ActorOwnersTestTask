// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FunctionalLibrary/OwnerTypes.h"
#include "ExampleActor.generated.h"

UCLASS()
class ACTOROWNERSTESTTASK_API AExampleActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AExampleActor();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Mesh)
	UStaticMeshComponent* ExampleMesh = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ActorOwner)
	FName DefaultOwnerID;
	UPROPERTY(BlueprintReadWrite)
	class UExampleActorSaveGame* ExampleActorSaveObject = nullptr;
	UFUNCTION(BlueprintCallable)
	void SaveExampleActor();

private:

	void CheckSaveGame();
};
