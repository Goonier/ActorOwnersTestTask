// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "FunctionalLibrary/OwnerTypes.h"
#include "TestTaskGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class ACTOROWNERSTESTTASK_API UTestTaskGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
	public:
	UTestTaskGameInstance();

	FOwnerInfo FindOwnerInfoByID(FName ID);

	UPROPERTY(BlueprintReadWrite)
	class UActorOwnersSaveData* ActorOwnersSaveObject = nullptr;

	UFUNCTION(BlueprintPure)
	bool OwnerIdExist(FName OwnerID);

	UFUNCTION(BlueprintCallable)
	void RemoveOwner(FOwnerInfo OwnerToRemove);

	UFUNCTION(BlueprintCallable)
	void AddNewOwner(FOwnerInfo NewOwner);

	protected:
	virtual void Init();

	private:

	void CheckSaveInfo();

	class UDataTable* DefaultOwnersTable = nullptr;

	TArray <FOwnerInfo> GetAllDefaultOwnersInfo();

	FString ActorOwnersSaveString = "OwnersSaveData";
};
