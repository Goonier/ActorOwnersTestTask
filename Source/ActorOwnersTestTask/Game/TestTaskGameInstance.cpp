// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/TestTaskGameInstance.h"
#include "UObject/ConstructorHelpers.h"
#include "ExampleActor/ExampleActor.h"
#include "Game/Save/ExampleActorSaveGame.h"
#include "Game/Save/ActorOwnersSaveData.h"
#include "Kismet/GameplayStatics.h"

UTestTaskGameInstance::UTestTaskGameInstance()
{
	//Set DefaultOwnersTable
	static ConstructorHelpers::FObjectFinder<UDataTable> TableOwnerObj
	(TEXT("/Game/ActorOwnersTestTask/Core/DataTable/DT_DefaultOwners"));

	DefaultOwnersTable = TableOwnerObj.Object;
}

FOwnerInfo UTestTaskGameInstance::FindOwnerInfoByID(FName ID)
{
	FOwnerInfo InfoFound;

	if (ActorOwnersSaveObject)
	{
		 TArray<FOwnerInfo> OwnersInfoArray = ActorOwnersSaveObject->Owners;
		 for (FOwnerInfo Info : OwnersInfoArray)
		 {
			 if (Info.ID == ID)
			 {
				InfoFound = Info;
				break;
			 }
		 }
	}

	return InfoFound;
}

bool UTestTaskGameInstance::OwnerIdExist(FName OwnerID)
{
	if (ActorOwnersSaveObject)
	{
		for (FOwnerInfo CurrentOwnerInfo : ActorOwnersSaveObject->Owners)
		{
			if (CurrentOwnerInfo.ID == OwnerID)
				return true;
		}
	}
	return false;
}

void UTestTaskGameInstance::RemoveOwner(FOwnerInfo OwnerToRemove)
{
	if (ActorOwnersSaveObject)
	{
		TArray<AActor*> ActorInLevel;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AExampleActor::StaticClass(), ActorInLevel);

		for (AActor* CurrentActor : ActorInLevel)
		{
			auto Example = Cast<AExampleActor>(CurrentActor);
			if (Example && Example->ExampleActorSaveObject->OwnerInfo.ID == OwnerToRemove.ID)
			{
				Example->ExampleActorSaveObject->OwnerInfo = FOwnerInfo();
			}
		}

		for (int32 Index = 0; Index <= ActorOwnersSaveObject->Owners.Num() - 1; Index++)
		{
			if (ActorOwnersSaveObject->Owners[Index].ID == OwnerToRemove.ID)
			{
				ActorOwnersSaveObject->Owners.RemoveAt(Index);
				UGameplayStatics::SaveGameToSlot(ActorOwnersSaveObject, ActorOwnersSaveString, 0);
				break;
			}
		}
	}
}

void UTestTaskGameInstance::AddNewOwner(FOwnerInfo NewOwner)
{
	if (ActorOwnersSaveObject)
	{
		ActorOwnersSaveObject->Owners.Add(NewOwner);
		UGameplayStatics::SaveGameToSlot(ActorOwnersSaveObject, ActorOwnersSaveString, 0);
	}
}

void UTestTaskGameInstance::Init()
{
	CheckSaveInfo();
}

void UTestTaskGameInstance::CheckSaveInfo()
{
	bool SaveGameExist = UGameplayStatics::DoesSaveGameExist(ActorOwnersSaveString, 0);

	if (SaveGameExist)
	{
		auto SaveGame = Cast<UActorOwnersSaveData>(UGameplayStatics::LoadGameFromSlot(ActorOwnersSaveString, 0));
		if (SaveGame)
		{
			ActorOwnersSaveObject = SaveGame;
		}
	}
	else
	{
		ActorOwnersSaveObject = Cast<UActorOwnersSaveData>(UGameplayStatics::CreateSaveGameObject(UActorOwnersSaveData::StaticClass()));
		ActorOwnersSaveObject->Owners = GetAllDefaultOwnersInfo();
		UGameplayStatics::SaveGameToSlot(ActorOwnersSaveObject, ActorOwnersSaveString, 0);
	}
}

TArray<FOwnerInfo> UTestTaskGameInstance::GetAllDefaultOwnersInfo()
{
	TArray<FOwnerInfo> OwnersInfoArray;

	if (DefaultOwnersTable)
	{
		TArray<FName> TableNames = DefaultOwnersTable->GetRowNames();
		FOwnerInfo* OwnerInfoRow;

		for (FName Name : TableNames)
		{
			OwnerInfoRow = DefaultOwnersTable->FindRow<FOwnerInfo>(Name, "", false);
			if (OwnerInfoRow)
			{
				OwnersInfoArray.Add(*OwnerInfoRow);
			}
		}
	}
	return OwnersInfoArray;
}
