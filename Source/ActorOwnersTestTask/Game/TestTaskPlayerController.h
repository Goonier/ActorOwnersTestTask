// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TestTaskPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class ACTOROWNERSTESTTASK_API ATestTaskPlayerController : public APlayerController
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;

public:
	ATestTaskPlayerController();

	UPROPERTY(BlueprintReadWrite)
	UUserWidget* IngameWidget = nullptr;

	UFUNCTION(BlueprintCallable)
	void MouseInput(bool NotVisible, bool& MouseVisibility);
private:
	//Ingame Widget class pointer
	TSubclassOf<UUserWidget> WidgetClass = nullptr;
};
