// Copyright Epic Games, Inc. All Rights Reserved.

#include "ActorOwnersTestTaskGameMode.h"
#include "Character/ActorOwnersTestTaskCharacter.h"
#include "TestTaskPlayerController.h"
#include "UObject/ConstructorHelpers.h"

AActorOwnersTestTaskGameMode::AActorOwnersTestTaskGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/ActorOwnersTestTask/Core/Character/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// Use our custom PlayerController class
	PlayerControllerClass = ATestTaskPlayerController::StaticClass();
}
