// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/TestTaskPlayerController.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"

ATestTaskPlayerController::ATestTaskPlayerController()
{
	//Set Ingame Widget
	static ConstructorHelpers::FClassFinder<UUserWidget> IngameWidgetObj
	(TEXT("/Game/ActorOwnersTestTask/Core/UI/WBP_Ingame"));
	
	WidgetClass = IngameWidgetObj.Class;
}

void ATestTaskPlayerController::MouseInput(bool NotVisible, bool& MouseVisibility)
{
    if (NotVisible)
    {
        FInputModeGameOnly GameOnlyMode;

        SetShowMouseCursor(false);
        SetInputMode(GameOnlyMode);
        MouseVisibility = false;
    }
    else
    {
        FInputModeGameAndUI GameAndUIMode;
        GameAndUIMode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
        GameAndUIMode.SetHideCursorDuringCapture(true);

        SetShowMouseCursor(true);
        SetInputMode(GameAndUIMode);
        MouseVisibility = true;
    }
}

void ATestTaskPlayerController::BeginPlay()
{
	if (WidgetClass)
	{
		IngameWidget = CreateWidget(this, WidgetClass);
		IngameWidget->AddToViewport();
	}
}
