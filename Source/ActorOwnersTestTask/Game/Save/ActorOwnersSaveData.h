// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "FunctionalLibrary/OwnerTypes.h"
#include "ActorOwnersSaveData.generated.h"

/**
 * 
 */
UCLASS()
class ACTOROWNERSTESTTASK_API UActorOwnersSaveData : public USaveGame
{
	GENERATED_BODY()
	
	public:

	UPROPERTY(BlueprintReadWrite)
	TArray<FOwnerInfo> Owners;
};
