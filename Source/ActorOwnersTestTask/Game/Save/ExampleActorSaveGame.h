// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "FunctionalLibrary/OwnerTypes.h"
#include "ExampleActorSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class ACTOROWNERSTESTTASK_API UExampleActorSaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintReadWrite)
	FOwnerInfo OwnerInfo;
};
