// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ActorOwnersTestTaskGameMode.generated.h"

UCLASS(minimalapi)
class AActorOwnersTestTaskGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AActorOwnersTestTaskGameMode();
};



