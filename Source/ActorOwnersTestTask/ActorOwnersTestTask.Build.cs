// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class ActorOwnersTestTask : ModuleRules
{
	public ActorOwnersTestTask(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicIncludePaths.AddRange(new string[] { "ActorOwnersTestTask" });

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "UMG" });
	}
}
