// Copyright Epic Games, Inc. All Rights Reserved.

#include "ActorOwnersTestTask.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ActorOwnersTestTask, "ActorOwnersTestTask" );
 