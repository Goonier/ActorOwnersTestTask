// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "OwnerTypes.generated.h"


UENUM(BlueprintType)
enum class EGender : uint8
{
	Male,
	Female
};

USTRUCT(BlueprintType)
struct FOwnerInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName ID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName Name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EGender Gender = EGender::Male;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D* Icon;
};

/**
 * 
 */
UCLASS()
class ACTOROWNERSTESTTASK_API UOwnerTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
