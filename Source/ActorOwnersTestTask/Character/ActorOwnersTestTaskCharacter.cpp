// Copyright Epic Games, Inc. All Rights Reserved.

#include "ActorOwnersTestTaskCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "Game/TestTaskPlayerController.h"
#include "Game/Save/ExampleActorSaveGame.h"
#include "Blueprint/UserWidget.h"
#include "Interface/UI_Helper.h"
#include "Kismet/GameplayStatics.h"
#include "ExampleActor/ExampleActor.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AActorOwnersTestTaskCharacter

AActorOwnersTestTaskCharacter::AActorOwnersTestTaskCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));
}

void AActorOwnersTestTaskCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	GetWorldTimerManager().SetTimer(SearchExampleActorHandle,
		this,
		&AActorOwnersTestTaskCharacter::SearchExampleActorTimer,
		0.01f,
		true);
}

//////////////////////////////////////////////////////////////////////////
// Input

void AActorOwnersTestTaskCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &AActorOwnersTestTaskCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AActorOwnersTestTaskCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AActorOwnersTestTaskCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AActorOwnersTestTaskCharacter::LookUpAtRate);

	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &AActorOwnersTestTaskCharacter::TryInteract);
}

AActor* AActorOwnersTestTaskCharacter::TraceInteractableActor() const
{
	FVector CameraLocation;
	FRotator CameraRotation;

	Controller->GetPlayerViewPoint(CameraLocation, CameraRotation);
	FVector EndLocation = CameraLocation + CameraRotation.Vector() * AimDistance;

	FHitResult AimHit;
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	GetWorld()->LineTraceSingleByChannel(
		AimHit,
		CameraLocation,
		EndLocation,
		ECollisionChannel::ECC_GameTraceChannel1,
		Params
	);

	AActor* HitActor = Cast<AActor>(AimHit.Actor);
	return HitActor;
}

void AActorOwnersTestTaskCharacter::ReturnControl()
{
	auto PlayerController = Cast<ATestTaskPlayerController>(GetController());

	if (PlayerController)
		PlayerController->MouseInput(true, InInteract);

	GetWorldTimerManager().UnPauseTimer(SearchExampleActorHandle);
}

void AActorOwnersTestTaskCharacter::SearchExampleActorTimer()
{
	AExampleActor* Actor = Cast<AExampleActor>(TraceInteractableActor());
	if (Actor)
	{
		UUserWidget* IngameWidget = GetInGameWidget();
		if (IngameWidget)
		{
			Cast<IUI_Helper>(IngameWidget)->Execute_ShowActorOwnerInfo(IngameWidget, Actor->ExampleActorSaveObject->OwnerInfo);
		}
	}
}

UUserWidget* AActorOwnersTestTaskCharacter::GetInGameWidget()
{
	UUserWidget* FoundWidget = nullptr;
	auto PlayerController = Cast<ATestTaskPlayerController>(GetController());

	if (PlayerController)
		FoundWidget = PlayerController->IngameWidget;

	return FoundWidget;
}

void AActorOwnersTestTaskCharacter::MoveForward(float Value)
{
	if (InInteract) { return; }

	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AActorOwnersTestTaskCharacter::MoveRight(float Value)
{
	if (InInteract) {return;}

	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AActorOwnersTestTaskCharacter::TryInteract()
{
	if (InInteract) {return;}

	AExampleActor* Actor = Cast<AExampleActor>(TraceInteractableActor());

	if (Actor)
	{
		UUserWidget* IngameWidget = GetInGameWidget();
		if (IngameWidget)
		{
			Cast<IUI_Helper>(IngameWidget)->Execute_StartInteractWithActorExample(IngameWidget, Actor);
			auto PlayerController = Cast<ATestTaskPlayerController>(GetController());
			if (PlayerController)
				PlayerController->MouseInput(false, InInteract);
			GetWorldTimerManager().PauseTimer(SearchExampleActorHandle);
		}
	}
}

void AActorOwnersTestTaskCharacter::TurnAtRate(float Rate)
{
	if (InInteract) { return; }

	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AActorOwnersTestTaskCharacter::LookUpAtRate(float Rate)
{
	if (InInteract) { return; }

	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}
